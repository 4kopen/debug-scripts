#!/bin/bash
#"================="
#  Restrictions
#    - Hack in transcode case : force encode ouput to memsink without checking it /!\ to be updated when tunneling mode btw encode and tsmux exists
#    - Subtitle not supported
#    - except thanks to media-ctl services, v4l2 devices are not shown
#

STKPI_INFO_PATH="/sys/kernel/stkpi/instance"
GET_INFO_MEDIACTRL="media-ctl -p"

#To change below colors, refer to http://www.graphviz.org/doc/info/colors.html
COLOR_PLAYBACK="dodgerblue"
COLOR_ENCODE="darkolivegreen1"
COLOR_DEMUX="sienna1"

function display_help {
  echo
  echo "###########################################################"
  echo
  echo "Usage : $1 BOARD_IPADDRESS [ loop ] [ config_file ]"
  echo
  echo "    - BOARD_IPADDRESS (mandatory) : IP address of the board."
  echo "      Will be used to establish connection between the host and the "
  echo "      board"
  echo "    - loop (optional) : allows to execute the script periodically "
  echo "      (only store graph (graph_[date].png) in current folder if they are different)"
  echo "    - configfile (optional) : by default dvbgraph.conf but can be another if it respects syntax"
  echo " "
  echo "Note : if dvbgraph.conf doesn't exist, script creates it with default values : "
  echo "  TELNET_CONNECTION=0	// mean ssh usage by default "
  echo "  NFS_TARGETFS_PATH=<TO BE UPDATED IN CASE OF TELNET USAGE> // only used in case of telnet"
  echo "###########################################################"
  echo
  exit
}


function return_PlaybackId {
  # param1 : file
  # param2 : dvb device
  echo "> return_PlaybackId ($1 $2 )" >> /tmp/debug.txt
  
  if [ $2 == "" ]; then
    echo "Invalid dvb device"
    echo "< return_PlaybackId [ no playback ]" >> /tmp/debug.txt
    return 0
  fi
  
  playbacks_available=`cat $1 | cut -f6 -d"/" | uniq | grep playback`
  
  if [ "$playbacks_available" == "" ]; then
    echo "No playback on going"
    echo "< return_PlaybackId [ no playback ]" >> /tmp/debug.txt
    return 0
  fi
  for playback in `echo "$playbacks_available"`
  do
    dvbs=$(eval "cat /tmp/stkpi_instance.txt | grep "$playback/" | cut -f7 -d"/" | uniq | egrep -e '^$2'")
    if [ "$dvbs" != "" ]; then
      echo $playback
      echo "< return_PlaybackId [ return \"$playback\" ]" >> /tmp/debug.txt
      return 1
    fi
  done
  echo "no playback instance found"
  echo "< return_PlaybackId [ no playback ]" >> /tmp/debug.txt
  return 0
}

function createClusterPlaybackDemux {
  # param1 : input file
  # param2 : output file
  # param3 : playback device name
  # param4 : cluster Id
  
  playbacks_available=`cat $1 | cut -f6 -d"/" | uniq | grep playback`
  echo "> createClusterPlaybackDemux ($1 $2 $3 $4)" >> /tmp/debug.txt
  if [ "$playbacks_available" == "" ]; then
    echo "No playback on going"
    echo "< createClusterPlaybackDemux [no playback]" >> /tmp/debug.txt
    return 0
  fi
  for playback in `echo "$playbacks_available"`
  do
    if [ "$playback" == "$3" ]; then
      dvbs=$(eval "cat $1 | grep "$playback/" | cut -f7 -d"/" | uniq | egrep -e '^dvb[0-9].audio[0-9]|^dvb[0-9].video[0-9]'")
      if [ "$dvbs" == "" ]; then
        echo "}" >> $2
        echo "< createClusterPlaybackDemux [no dvb devices]" >> /tmp/debug.txt
        return 0
      fi
      echo "subgraph cluster$4 {" >> $2
      echo "node [style=filled,color=white];" >> $2
      echo "	style=filled;" >> $2
      for dvb in `echo $dvbs`
      do
        echo "	\"$dvb\""  >> $2
        echo "$playback => cluster $4 => $dvb device added " >> /tmp/debug.txt
      done
      echo "$playback:$4" >> /tmp/pb_cluster_links.txt
      echo "	label=\"$3\"" >> $2
      echo "	color=$COLOR_PLAYBACK " >> $2
      echo "}" >> $2
    fi
  done
  echo "< createClusterPlaybackDemux [ok]" >> /tmp/debug.txt
  return 1
}

function lookForClusterId {
  #param 1 file
  #param 2 key
  while read line
  do
    key=$(echo "$line" | cut -f1 -d":")
    if [ "$key" == "$2" ]; then
      value=$(echo "$line" | cut -f2 -d":")
      echo $value
      return 1
    fi
  done < $1
  echo "not_found"
  return 0
}

function createClusterPlayback {
  # param1 : input file
  # param2 : output file
  # param3 : cluster Id
  #!!! take care no echo in this service in order to return the new cluster id
  echo "> createClusterPlayback ($1 $2 $3)" >> /tmp/debug.txt
  indexCluster=$3
  playbacks_available=`cat $1 | cut -f6 -d"/" | uniq | grep playback`
  if [ "$playbacks_available" == "" ]; then
    echo $indexCluster
    echo "< createClusterPlayback [no playback return $indexCluster]" >> /tmp/debug.txt
    return 0
  fi
  index=0
  for playback in `echo "$playbacks_available"`
  do
    testFound=$(lookForClusterId /tmp/pb_cluster_links.txt $playback)
    if [ "$testFound" == "not_found" ]; then
      echo "Check $playback " >> /tmp/debug.txt
      echo "subgraph cluster$indexCluster {" >> $2
      echo "node [style=filled,color=white];" >> $2
      echo "	style=filled;" >> $2
      dvbs=$(eval "cat $1 | grep "$playback/" | cut -f7 -d"/" | uniq | egrep -e '^dvb[0-9].audio[0-9]|^dvb[0-9].video[0-9]'")
      if [ "$dvbs" != "" ]; then
        for dvb in `echo $dvbs`
        do
          echo "	\"$dvb\""  >> $2
          echo "$playback => cluster $indexCluster => $dvb device added " >> /tmp/debug.txt
        done
      else
        echo "		\"Unknown\"" >> $2
      fi
      echo "	label=\"$playback\"" >> $2
      echo "	color=$COLOR_PLAYBACK " >> $2
      echo "}" >> $2
      echo "$playback:$indexCluster" >> /tmp/pb_cluster_links.txt
      if [ ! -z $dvb ]; then
        echo "\"memsource$indexCluster$index\" -> \"$dvb\" [lhead=cluster$indexCluster];" >> $2
        let "index++"
      fi
      let "indexCluster++"
    fi
  done
  echo $indexCluster
  echo "< createClusterPlayback [ ok return $indexCluster]" >> /tmp/debug.txt
  return 1
}

function createClusterEncode {
  # param1 : input file
  # param2 : output file
  # param3 : cluster Id
  #!!! take care no echo in this service in order to return the new cluster id
  echo "> createClusterEncode ($1 $2 $3)" >> /tmp/debug.txt
  indexCluster=$3
  encoders_available=`cat $1 | cut -f6 -d"/" | uniq | grep encode`
  
  if [ "$encoders_available" == "" ]; then
    echo $indexCluster
    echo "< createClusterEncode [no encoder return $indexCluster]" >> /tmp/debug.txt
    return 0
  fi
  for encoder in `echo "$encoders_available"`
  do
    echo "subgraph cluster$indexCluster {" >> $2
    echo "	node [style=filled,color=white];" >> $2
    echo "	style=filled;" >> $2
    
    links=$(eval "cat $1 | grep "$encoder/" | cut -f8 -d"/" | uniq | grep dvb0")
    echo $links
    itemEncode=1
    for link in `echo $links`
    do
      source=$(echo $link | cut -f4 -d'-')
      dest=$(echo $link | cut -f2 -d'-')
      #Patch to match with media-ctl name
      if [ "$dest" == "EncAudStream00" ]; then
        dest="aud-encoder-00"
      fi
      if [ "$dest" == "EncVidStream00" ]; then
        dest="vid-encoder-00"
      fi
      echo "	\"$source\" -> \"$dest\""  >> /tmp/$encoder"_link".txt
      echo "	\"$dest\" -> \"memsink$indexCluster$itemEncode\""  >> /tmp/$encoder"_link".txt
      echo "	\"$dest\""  >> $2
      echo "$encoder => cluster $indexCluster => $dest added " >> /tmp/debug.txt
      let "itemEncode++"
    done
    echo "	label=\"$encoder\"" >> $2
    echo "	color=$COLOR_ENCODE " >> $2
    echo "}" >> $2
    cat /tmp/$encoder"_link".txt >> $2
    
    rm /tmp/$encoder"_link".txt
    let "indexCluster++"
  done
  echo $indexCluster
  echo "< createClusterEncode [return $indexCluster]" >> /tmp/debug.txt
  return 1
}

function createClusterTsMux {
  # param1 : input file
  # param2 : output file
  tsmuxs_available=`cat $1 | cut -f6 -d"/" | grep tsmux | cut -f1 -d"-" | uniq`
  if [ "$tsmuxs_available" != "" ]; then
    for tsmux in `echo "$tsmuxs_available"`
    do
      echo "	\"$tsmux\"" >> $2
    done
  fi
  return 1
}


function ssh_connection {
  echo "... SSH Connection on going ..."
  eval "ssh root@$IPADDRESS 'ls'" >& /dev/null
  if [ $? != 0 ]; then
    echo
    echo "/!\ ssh connection issue : check the validity of the IP address /!\\"
    echo
    exit
  fi
  
  echo "" > /tmp/debug.txt
  echo "[main] clean-up temporary files" >> /tmp/debug.txt
  echo "[main] retrieve /sys/kernel/stkpi/instance" >> /tmp/debug.txt
  
ssh root@$IPADDRESS <<'ENDSSH'  >& /dev/null
	find /sys/kernel/stkpi/instance/ > /usr/stkpi_instance.txt
	if [ $? != 0 ]; then
	   exit $?
	fi
	cat /usr/stkpi_instance.txt | egrep -e ".*/pid" > /usr/temp_pid.txt
	while read line
	do
		echo "$line/dvbgraph_value=`cat $line`" >> /usr/stkpi_instance.txt
	done < 	/usr/temp_pid.txt
	rm /usr/temp_pid.txt
	media-ctl -p > /usr/media_ctl.txt
ENDSSH
  if [ $? != 0 ]; then
    echo
    echo "!!! Impossible to access to $STKPI_INFO_PATH"
    echo
    echo "[main] Impossible to access to $STKPI_INFO_PATH" >> /tmp/debug.txt
    exit
  fi
  scp root@$IPADDRESS:/usr/stkpi_instance.txt /tmp/stkpi_instance.txt >& /dev/null
  scp root@$IPADDRESS:/usr/media_ctl.txt /tmp/media_ctl.txt >& /dev/null
}

function generate_TelnetEmbeddedScript {
  #param 1 : script path+name
  echo "#!/bin/sh" > $1
  echo "sleep 1" >> $1
  echo "echo 'root'" >> $1
  echo "sleep 1" >> $1
  echo "echo 'find /sys/kernel/stkpi/instance > /usr/stkpi_instance.txt'" >> $1
  echo "sleep 1" >> $1
  echo "echo 'rm -rf /usr/temp_pid.txt'" >> $1
  echo "echo 'for i in \`cat /usr/stkpi_instance.txt | egrep -e '.*/pid'\`; do echo -n \"\$i/dvbgraph_value=\" >> /usr/temp_pid.txt ; echo \`cat \$i\` >> /usr/temp_pid.txt ; done'"  >> $1
  echo "sleep 1" >> $1
  echo "echo 'media-ctl -p > /usr/media_ctl.txt'" >> $1
  echo "sleep 1" >> $1
  echo "echo 'logout'" >> $1
}

function telnet_connection {
  #param 1 : path to reach targetfs in nfs
  NFS_ROOTFS_PATH=$1
  generate_TelnetEmbeddedScript /tmp/ebd.sh
  echo "... Telnet Connection on going ..."
  sh /tmp/ebd.sh | telnet $IPADDRESS
  rm /tmp/ebd.sh
  
  if [ -f $NFS_ROOTFS_PATH/usr/stkpi_instance.txt ]; then
    cp $NFS_ROOTFS_PATH/usr/stkpi_instance.txt /tmp/stkpi_instance.txt
  else
    echo ""; echo " /!\ ERROR : Information (stkpi_instance) not received /!\ "; echo ""
    exit
  fi
  
  if [ -f $NFS_ROOTFS_PATH/usr/media_ctl.txt ]; then
    cp $NFS_ROOTFS_PATH/usr/media_ctl.txt  /tmp/media_ctl.txt
  else
    echo ""; echo " /!\ ERROR : Information (media_ctl) not received /!\ "; echo ""
    exit
  fi
  
  if [ -f $NFS_ROOTFS_PATH/usr/temp_pid.txt ]; then
    cp $NFS_ROOTFS_PATH/usr/temp_pid.txt  /tmp/temp_pid.txt
    cat /tmp/temp_pid.txt >> /tmp/stkpi_instance.txt
  else
    echo ""; echo " /!\ Warning : Information (pid) not received /!\ "; echo ""
  fi
}

#####################  MAIN PART ###################
if [ -z $1 ]; then
  echo
  echo "ERROR : Please specify IP address of the board as parameter"
  display_help $0
else
  if [ "$1" == "-h" ]; then
    display_help $0
  else
    IPADDRESS=$1
  fi
fi
path=`dirname $0`
doLoop="false"
if [ ! -z $2 ]; then
  if [ "$2" == "loop" ]; then
    echo "Loop mode enabled - Ctrl-C to quit"
    doLoop="true"
    nb_loop=0
  else
    echo "Loop mode disabled"
    if [ -f $2 ]; then
      configFile=$2
    else
      echo ""; echo " /!\ Configuration file given in parameter not found /!\ "; echo ""
      exit
    fi
  fi
  if [ ! -z $3 ]; then
    configFile=$3
  fi
fi
if [ ! -z $configFile ] ; then
  if [ ! -f $configFile ]; then
    echo ""; echo " /!\ Configuration file given in parameter not found /!\ "; echo ""
    exit;
  fi
else
  configFile=$path/dvbgraph.conf
  if [ ! -f $path/dvbgraph.conf ]; then
    echo "TELNET_CONNECTION=0" > $path/dvbgraph.conf
    echo "NFS_TARGETFS_PATH=<TO BE UPDATED IN CASE OF TELNET USAGE>" >> $path/dvbgraph.conf
  fi
fi

#By default, SSH is used to connect the board.
isTelnetConnection=0
if [ -f $configFile ]; then
  echo "Configuration file <$configFile> is used"
  while read line
  do
    key=`echo $line | cut -f1 -d"="`
    value=`echo $line | cut -f2 -d"="`
    if [ "$key" != "" ]; then
      case $key in
        "TELNET_CONNECTION") isTelnetConnection=$value;;
        "NFS_TARGETFS_PATH") nfs_path="$value";;
        *) echo "==> tag $key not supported";;
      esac
    fi
  done < $configFile
else
  echo "No configuration file found : SSH connection per default"
fi

if [ $isTelnetConnection != 0 ]; then
  if [ -z $nfs_path ] || [ ! -d "$nfs_path" ] ; then
    echo ""; echo " /!\ NFS path not well defined in config file /!\ "; echo ""
    display_help $0
    exit;
  fi
fi

known_pidfilters=("PESFilter" "PCRFilter" "TSFilter" "TSIdxFilter" "SectionFilter")

if [ -f previous_graph.dot ]; then
  rm previous_graph.dot
fi
if [ -f graph.dot ] ; then
  rm graph.dot
fi
if [ -f graph.png ] ; then
  rm graph.png
fi

while true
do
  echo "Retrieve information from the board ... "
  if [ $isTelnetConnection = 1 ]; then
    telnet_connection $nfs_path
  else
    ssh_connection
  fi
  echo "Done"
  
  # Initialize temporary files
  echo "" > /tmp/info_demux_connect.txt
  echo "" > /tmp/clusterDes.txt
  echo "" > /tmp/info_playback.txt
  echo "" > /tmp/info_encode.txt
  echo "" > /tmp/info_tsmux.txt
  echo "" > /tmp/pb_cluster_links.txt
  
  # Parse /sys/kernel/stkpi/instance folder to detect demux usage
  demuxs_available=`cat /tmp/stkpi_instance.txt | cut -f6 -d"/"  | grep stm_dvb_demux | sort -u`
  echo "[main] demux list : $demuxs_available" >> /tmp/debug.txt
  dot_subgraph_item=0
  for demux in `echo "$demuxs_available"`
  do
    previousDev=""
    demuId=`echo "$demux" | cut -f2 -d'.'`
    echo ""  >> /tmp/debug.txt
    echo "[main] check demux $demux Id=$demuId" >> /tmp/debug.txt
    # Try to detect source of the demux
    previousDevFull=$(eval "cat /tmp/stkpi_instance.txt | grep "$STKPI_INFO_PATH/$demux/backlink"")
    frontend_devices=([1]="stm_fe_ip" [2]="stm_fe_demod")
    for fe_dev in "${frontend_devices[@]}"
    do
      if [[ $previousDevFull = *"$fe_dev"* ]]; then
        previousDev=$fe_dev
        previousDevId=`echo $previousDevFull | sed -e "s/.*stm_fe_[a-z]*\.\([0-9]\).*/\1/g"`
        break
      fi
    done
    if [ "$previousDev" == "" ]; then
      previousDev="memsource"
      previousDevId=$demuId
    fi
    echo "[main] $previousDev$demuId detected before demux" >> /tmp/debug.txt
    padSourceNb=1
    echo "subgraph cluster$dot_subgraph_item {" >> /tmp/clusterDes.txt
    echo "node [style=filled,color=white];" >> /tmp/clusterDes.txt
    echo "	style=filled;" >> /tmp/clusterDes.txt
    echo "	color=$COLOR_DEMUX;" >> /tmp/clusterDes.txt
    # Then try to identify content of demux and output links
    PIDFilters_available=$(eval "cat /tmp/stkpi_instance.txt | grep "$demux" | cut -f7 -d"/" | grep Pid.Filter | sort -u")
    if [ "$PIDFilters_available" != "" ]; then
      sourceIsLinked=""
      pcr_filterDetected=0
      storeDvbToConnectPCR=""
      storePCRhexPID=""
      for Pidfilter in `echo $PIDFilters_available`
      do
        echo "   PIDFilter : $Pidfilter" >> /tmp/debug.txt
        infos=$(eval "cat /tmp/stkpi_instance.txt | grep "$demux/$Pidfilter" | cut -f8 -d"/" | grep output | sort -u")
        for info in `echo $infos`
        do
          infoId=`echo $info | sed -e 's/.*output(\([0-9a-z][0-9a-z][0-9a-z][0-9a-z][0-9a-z][0-9a-z]\)[0-9a-z][0-9a-z])/\1/g'`
          echo -n "       infoId : $infoId  => " >> /tmp/debug.txt
          # Retrieve PID Filter type
          count=${#known_pidfilters[@]}
          index=0
          typePidFilter="unknown"
          while [ "$index" -lt "$count" ]; do
            res=$(eval "cat /tmp/stkpi_instance.txt | egrep -e "$demux/${known_pidfilters[$index]}.$infoId" | cut -f7 -d"/" | sort -u")
            if [ "$res" != "" ]; then
              typePidFilter=${known_pidfilters[$index].$infoId}
              index=10
            fi
            let "index++"
          done
          echo $typePidFilter >> /tmp/debug.txt
          pid=$(cat /tmp/stkpi_instance.txt | egrep -e ".*/$demux/$Pidfilter/pid/dvbgraph_value" | sed -e 's/.*dvbgraph_value=\([0-9]*\)/\1/g')
          hexpid=`printf "0x%.4x" $pid`
          if [ "$pid" != "" ]; then
            echo "		====> Pid = $pid" >> /tmp/debug.txt
          fi
          #update graph info according to Pid filter type
          case $typePidFilter in
            "PCRFilter")
              echo "	\"$typePidFilter$demuId\n(pid:$hexpid)\"" >> /tmp/clusterDes.txt
              if [ "$sourceIsLinked" == "" ]; then
                echo "	\"$previousDev$previousDevId\" -> \"$typePidFilter$demuId\n(pid:$hexpid)\" [lhead=cluster$dot_subgraph_item];" >> /tmp/info_demux_connect.txt
                sourceIsLinked="done"
              fi
              storePCRhexPID=$hexpid
              pcr_filterDetected=1
            ;;
            "PESFilter")
              dvb_device=$(cat /tmp/stkpi_instance.txt | egrep -e "$demux/$typePidFilter.$infoId[0-9][0-9]/dvb" | sed -e 's/.*\(dvb[0-9].*\)/\1/g')
              echo "	\"$typePidFilter$demuId\n(pid:$hexpid)\"" >> /tmp/clusterDes.txt
              if [ "$sourceIsLinked" == "" ]; then
                echo "	\"$previousDev$previousDevId\" -> \"$typePidFilter$demuId\n(pid:$hexpid)\" [lhead=cluster$dot_subgraph_item];" >> /tmp/info_demux_connect.txt
                sourceIsLinked="done"
              fi
              if [ "$dvb_device" == "" ]; then
                dest=$(eval "cat /tmp/stkpi_instance.txt | egrep -e "from-$typePidFilter.$infoId"  | cut -f6 -d"/" | cut -f1 -d":" | sort -u")
                if [ "$dest" != "" ]; then
                  echo "	\"$typePidFilter$demuId\n(pid:$hexpid)\" -> \"$dest\"" >> /tmp/info_demux_connect.txt
                fi
              else
                echo "	\"$typePidFilter$demuId\n(pid:$hexpid)\" -> \"$dvb_device\"" >> /tmp/info_demux_connect.txt
                storeDvbToConnectPCR=$dvb_device
              fi
            ;;
            "SectionFilter")
              #we replace PID by corresponding tables
              sectionKind=""
              case $hexpid in
                "0x0000")
                  sectionKind="PAT"
                ;;
                "0x0001")
                  sectionKind="CAT"
                ;;
                "0x0002")
                  sectionKind="TSDT"
                ;;
                "0x0010")
                  sectionKind="NIT"
                ;;
                "0x0011")
                  sectionKind="SDT/BAT"
                ;;
                "0x0012")
                  sectionKind="EIT"
                ;;
                "0x0013")
                  sectionKind="RST"
                ;;
                "0x0014")
                  sectionKind="TDT/TOT"
                ;;
                "0x0015")
                  sectionKind="Net synch"
                ;;
                "0x001E")
                  sectionKind="DIT"
                ;;
                "0x001F")
                  sectionKind="SIT"
                ;;
              esac
              dest=$(eval "cat /tmp/stkpi_instance.txt | egrep -e "from-$typePidFilter.$infoId"  | cut -f6 -d"/" | cut -f1 -d":" | sort -u")
              if [ "$sectionKind" != "" ]; then
                echo "	\"$typePidFilter$demuId\n(pid:$hexpid-$sectionKind)\"" >> /tmp/clusterDes.txt
                if [ "$dest" != "" ]; then
                  echo "	\"$typePidFilter$demuId\n(pid:$hexpid-$sectionKind)\" -> \"$dest\"" >> /tmp/info_demux_connect.txt
                fi
                if [ "$sourceIsLinked" == "" ]; then
                  echo "	\"$previousDev$previousDevId\" -> \"$typePidFilter$demuId\n(pid:$hexpid-$sectionKind)\" [lhead=cluster$dot_subgraph_item];" >> /tmp/info_demux_connect.txt
                  sourceIsLinked="done"
                fi
              else
                echo "	\"$typePidFilter$demuId\n(pid:$hexpid)\"" >> /tmp/clusterDes.txt
                if [ "$dest" != "" ]; then
                  echo "	\"$typePidFilter$demuId\n(pid:$hexpid)\" -> \"$dest\"" >> /tmp/info_demux_connect.txt
                fi
                if [ "$sourceIsLinked" == "" ]; then
                  echo "	\"$previousDev$previousDevId\" -> \"$typePidFilter$demuId\n(pid:$hexpid)\" [lhead=cluster$dot_subgraph_item];" >> /tmp/info_demux_connect.txt
                  sourceIsLinked="done"
                fi
              fi
            ;;
            "TSFilter" | "TSIdxFilter")
              echo "	\"$typePidFilter$demuId\n(pid:$hexpid)\"" >> /tmp/clusterDes.txt
              dest=$(eval "cat /tmp/stkpi_instance.txt | egrep -e "from-$typePidFilter.$infoId"  | cut -f6 -d"/" | cut -f1 -d":" | sort -u")
              if [ "$dest" != "" ]; then
                echo "	\"$typePidFilter$demuId\n(pid:$hexpid)\" -> \"$dest\"" >> /tmp/info_demux_connect.txt
              fi
            ;;
            *)
              echo "unknown PID Filter for $infoId" >> /tmp/debug.txt
              echo "!!! unknown PID Filter for $infoId !!!"
            ;;
          esac
          pid=""
        done
        let "padSourceNb++"
      done
      # Retrieve playback linked to demux
      if [ ! -z $storeDvbToConnectPCR ]; then
        playbackDeviceName=$(return_PlaybackId /tmp/stkpi_instance.txt $storeDvbToConnectPCR)
        let "dot_subgraph_item++"
        $(createClusterPlaybackDemux /tmp/stkpi_instance.txt /tmp/info_playback.txt $playbackDeviceName $dot_subgraph_item)
        if [ $pcr_filterDetected == 1 ]; then
          if [ $storeDvbToConnectPCR != "" ]; then
            echo "	\"PCRFilter$demuId\n(pid:$storePCRhexPID)\" -> \"$storeDvbToConnectPCR\" [lhead=cluster$dot_subgraph_item];" >> /tmp/info_demux_connect.txt
          else
            dest=$(eval "cat /tmp/stkpi_instance.txt | egrep -e "from-PCRFilter.$infoId"  | cut -f6 -d"/" | cut -f1 -d":" | sort -u")
            if [ "$dest" != "" ]; then
              echo "	\"PCRFilter$demuId\n(pid:$storePCRhexPID)\" -> \"$dest\"" >> /tmp/info_demux_connect.txt
            fi
          fi
        fi
      fi
    else
      echo "Warning $demux without any PID filter"
      echo "Warning $demux without any PID filter" >> /tmp/debug.txt
      echo "	\"Unknown$demuId\"" >> /tmp/clusterDes.txt
    fi
    echo "	label=\"demux$demuId\"" >> /tmp/clusterDes.txt
    echo "}" >> /tmp/clusterDes.txt
    let "dot_subgraph_item++"
  done
  
  #check if standalone playback is used :
  echo ""  >> /tmp/debug.txt
  echo "[main] check potential additional playbacks" >> /tmp/debug.txt
  dot_subgraph_item=$(createClusterPlayback /tmp/stkpi_instance.txt /tmp/info_playback.txt $dot_subgraph_item)
  
  #check if encoder is used :
  echo ""  >> /tmp/debug.txt
  echo "[main] check potential additional encodes" >> /tmp/debug.txt
  dot_subgraph_item=$(createClusterEncode /tmp/stkpi_instance.txt /tmp/info_encode.txt $dot_subgraph_item)
  
  #check if tsmux is used :
  echo ""  >> /tmp/debug.txt
  echo "[main] check potential additional tsmux usage" >> /tmp/debug.txt
  $(createClusterTsMux /tmp/stkpi_instance.txt /tmp/info_tsmux.txt)
  
  ###################
  #MEDIA-CTL infos
  ###################
  
  echo ""  >> /tmp/debug.txt
  echo "[main] Retrieve info from media-ctl" >> /tmp/debug.txt
  #Parse remaining part of the graph from mediactrl services
  while read line
  do
    regex='(^- entity [0-9]+\:.*)'
    if [[ $line =~ $regex ]]; then
      entity=$(echo $line | cut -f2 -d":" | cut -f1 -d"(")
      entity=`echo $entity  | tr -d ' '`
    fi
    regex='.*<-.*\[ENABLED\].*'
    if [[ $line =~ $regex ]]; then
      source=$(echo $line | cut -f2 -d"\"")
      source=`echo $source  | tr -d ' '`
      echo "\""$source"\" -> \"$entity\"" >> /tmp/media_ctrl_1.txt
    fi
    regex='.*->.*\[ENABLED\].*'
    if [[ $line =~ $regex ]]; then
      dest=$(echo $line | cut -f2 -d"\"")
      dest=`echo $dest  | tr -d ' '`
      echo "\"$entity\" -> \"$dest\"" >> /tmp/media_ctrl_1.txt
    fi
  done < /tmp/media_ctl.txt
  
  if [ -f /tmp/media_ctrl_1.txt ]; then
    sort -u /tmp/media_ctrl_1.txt > /tmp/media_ctl.txt
  fi
  
  if [ -f graph.dot ] ; then
    if [ "$doLoop" == "true" ]; then
      cp ./graph.dot ./previous_graph.dot
    fi
    rm graph.dot
  fi
  
  ########## Graph generation ###########
  echo "digraph G {" >> graph.dot
  echo "	compound=true;">> graph.dot
  cat /tmp/clusterDes.txt >> graph.dot
  cat /tmp/info_playback.txt  >> graph.dot
  cat /tmp/info_demux_connect.txt  >> graph.dot
  cat /tmp/info_tsmux.txt >> graph.dot
  # in following command remove aud-encoder and vid-encoder from mediactl results : already handled before
  cat /tmp/media_ctl.txt | grep -v "aud-encoder" | grep -v "vid-encoder" >> graph.dot
  cat /tmp/info_encode.txt >> graph.dot
  echo "}"  >> graph.dot
  
  dot -Tpng graph.dot -o graph.png
  
  ########## Clean up ###########
  if [ -f /tmp/media_ctrl_1.txt ] ; then
    rm /tmp/media_ctrl_1.txt
  fi
  if [ -f /tmp/media_ctl.txt ] ; then
    rm /tmp/media_ctl.txt
  fi
  if [ -f /tmp/stkpi_instance.txt ] ; then
    rm /tmp/stkpi_instance.txt
  fi
  if [ -f /tmp/info_playback.txt ] ; then
    rm /tmp/info_playback.txt
  fi
  if [ -f /tmp/info_demux_connect.txt ] ; then
    rm /tmp/info_demux_connect.txt
  fi
  if [ -f /tmp/graph_media_ctrl.txt ] ; then
    rm /tmp/graph_media_ctrl.txt
  fi
  if [ -f /tmp/clusterDes.txt ] ; then
    rm /tmp/clusterDes.txt
  fi
  if [ -f /tmp/info_encode.txt ] ; then
    rm /tmp/info_encode.txt
  fi
  if [ -f /tmp/info_tsmux.txt ] ; then
    rm /tmp/info_tsmux.txt
  fi
  
  ######## Loop if needed ######
  if [ "$doLoop" == "true" ]; then
    filename="graph_`date "+%H_%M_%S"`.png"
    if [ -f previous_graph.dot ]; then
      resultat=$(diff previous_graph.dot graph.dot) >& /dev/null
      if [ ! -z "$resultat" ]; then
        echo "Update of the graph : store it in $filename file"
        cp graph.png $filename
        eog graph.png &
      fi
    else
      echo "Store initial graph in $filename file"
      cp graph.png $filename
      eog graph.png &
    fi
  else
    eog graph.png &
    exit
  fi
done
