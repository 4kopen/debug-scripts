Introduction
============
This is readme file to describe in details of sdk2_debug_trace.sh

What does the script do?
-----------------------
This script enables any sdk2 user to centrally control the debug traces across all sub systems including middle-ware. It allows to manage debug traces by executing this script with some simple arguments.

How to use the script?
---------------------
This script supports different arguments to micro manage different debug traces across multiple sub systems

Argument List:
-------------
--m
--module [=module_name]
		Module name can be any STKPI driver like fe(FrontEnd Engine),
		te(Transport Engine), se(Streaming Engine), de(Display Engine). It can
		also be any middle ware name like cdi or stv(STLinuxTv)
		Special module type, <all>, this option can be used to enable traces
		across whole sdk2.
		e.g: --module=all

--t
--type [=Type of Trace in the form of a string]

		This argument requires the the type of traces user expects. Types can be
		like api/avsync/injection etc. This argument is like keyword for the
		module to enable/disable certain specific traces.
                e.g. To enable API traces for all modules :
                          --module=all --type=api
                Like for a middle-ware, it can mean the device for which user wants to
                enable the traces.
		e.g: --module=stv --type=demux
--l
--level [=level of debug trace in the form of a numerical value]
		This argument can take any numerical value up to 5 provided the module
		also supports it.
		(0 = Disable, 1 = Error msg, 2 = Warning log, 3 = Info log(Default), 4 = Debug, 5 = Verbose)
		e.g: --level=3

--h
--help [shows this help]

Sample Usage
------------
1. Enable all level 1 API traces of transport engine module 
sdk2_debug_trace --module=te --type=api --level=1

2. Disable all traces
sdk2_debug_trace --module=all --type=api --level=0

3. See any sub-system help
sdk2_debug_trace --module=de --help
