#! /bin/bash

### compile the kernel with debug_fs and dynamic_debug on host window
###CONFIG_DEBUG_FS=y
###CONFIG_DYNAMIC_DEBUG=y

g_usage ()
{
        echo -e ""
        echo -e "  ${RED} Please ensure that modules are loaded before using this script ${NC} "
        echo -e "\n${GREEN}Usage${NC}:"
        echo -e "    sdk2_debug_trace [options]"
        echo -e "    Command to use: sdk2_debug_trace --module=<val> --type=<val> --level=<val> --help \n"
        echo -e "${GREEN}Options:${NC}"
        echo -e "  ${BLUE}--module=<val>${NC}"
        echo -e "    <val>=de or te or se or fe or stv or all. Specify the engine value to be used as te(transport engine), se(streami
ng engine), fe(frontend engine) and de(display engine).${RED} **Mandatory**${NC}\n"
        echo -e "  ${BLUE}--type=<val>${NC}"
        echo -e "    For example = api, avsync, se-pipeline, transcode, etc. ${RED}**Mandatory**${NC} \n "
        echo -e "  ${BLUE}--level=<val>${NC}"
        echo -e "    Starts printing level information about the state of the application.${RED} **Mandatory**${NC}"
        echo -e "    (0 = Disable, 1 = Error msg, 2 = Warning log, 3 = Info log(Default), 4 = Debug, 5 = Verbose)\n"
        echo -e "  ${BLUE}--help${NC}"
        echo -e "    Prints this information\n"
        echo -e "  ${GREEN} For individual sub-system help, use command: ${NC}sdk2_debug_trace --module=<value> --help "
}

NC='\e[0m'
GREEN='\e[0;92m'
RED='\e[0;31m'
BLUE='\033[0;34m'

drv_val=none
group_val=none
action_val=none

# options may be followed by one colon to indicate they have a required argument
if ! options=$(getopt -a -o m:t:l:h -l module:,type:,level:,help -- "$@")
then
    # something went wrong, getopt will put out an error message for us
        g_usage
#       break
fi

set -- $options
###assign arguments as argument $1 $2 ...

#streaming engine
se_file=/etc/debug_scripts/process_se_traces.sh
if [ -f  $se_file ]
then
source /etc/debug_scripts/process_se_traces.sh
fi

#display engine
de_file=/etc/debug_scripts/de_traces.sh
if [ -f $de_file ]
then
source /etc/debug_scripts/de_traces.sh
fi

#transport engine
te_file=/etc/debug_scripts/te_trace.sh
if [ -f $te_file ]
then
source /etc/debug_scripts/te_trace.sh
fi

#frontend
fe_file=/etc/debug_scripts/fe_trace.sh
if [ -f $fe_file ]
then
source /etc/debug_scripts/fe_trace.sh
fi

#stlinuxtv
stv_file=/etc/debug_scripts/process_stv_trace.sh
if [ -f $stv_file ]
then
source /etc/debug_scripts/process_stv_trace.sh
fi

#all engines
process_all_trace()
{
        drv_group=$1
        drv_level=$2
        for module in se_trace te_trace de_trace fe_trace
        do
                echo -e " $GREEN calling process_$module $NC"
                process_$module $drv_group $drv_level
        done
}

parse_parameters()
{
        drv_vl=$1
        group_vl=$2
        level_vl=$3
        if [[ $drv_vl == *"all"* ]]
        then
        echo "process_all_trace is being called"
        process_all_trace $group_vl $level_vl

        elif [[ $drv_vl == *"fe"* ]]
        then
        echo "process_fe_trace is being called"
        process_fe_trace $group_vl $level_vl

        elif [[ $drv_vl == *"se"* ]]
        then
        echo "process_se_trace is being called"
        process_se_trace $group_vl $level_vl

        elif [[ $drv_vl == *"te"* ]]
        then
        echo "process_te_trace is being called"
        process_te_trace $group_vl $level_vl

        elif [[ $drv_vl == *"stv"* ]]
        then
        echo "process_stv_trace is being called"
        process_stv_trace $group_vl $level_vl

        elif [[ $drv_vl == *"de"* ]]
        then
        echo "process_de_trace is being called"
        process_de_trace $group_vl $level_vl

        else
        echo -e "${RED} Wrong argument input ${NC}"
        echo -e "${RED} Use --help ${NC}"
        fi
}


while [ $# -gt 0 ]

do
        case $1 in

        --module)
                drv_val1=$2 ;
                drv_val=$(echo "$drv_val1" | sed -e "s/^'//"  -e "s/'$//") ;
                echo -e "${GREEN} module value is $drv_val ${NC}" ;
                shift;;

        --type)
                group_val1=$2 ;
                group_val=$(echo "$group_val1" | sed -e "s/^'//"  -e "s/'$//") ;
                echo -e "$GREEN type value is $group_val" $NC;
                shift;;

        --level)
                level_val1=$2 ;
                level_val=$(echo "$level_val1" | sed -e "s/^'//"  -e "s/'$//") ;
                echo -e "${GREEN} debug level value is $level_val ${NC}" ;
                shift;;

        --help)
                if [ -z $drv_val1 ];
                then
                g_usage;
                break;
                else
                process_${drv_val}_help
                fi
                break;;

        [?])
                g_usage;
                break;;
        esac
    shift
done

if [ -z $drv_val1  ];
then
echo
else
        if [ -z $level_val  ];
        then
        echo
        else
                if [ -z $group_val1 ];
                then
                echo -e "\n${RED} --type option is mandatory${NC}"
                echo -e " Please see usage using${BLUE} --help${NC} option \n\n"
                else
                parse_parameters $drv_val $group_val $level_val
                fi
        fi
fi


